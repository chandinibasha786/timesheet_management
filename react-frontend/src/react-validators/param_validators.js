/**
 * Helper function to validate name string
 * checks if name is string, else returns "name should be of string type"
 * checks if name is too short, else returns "name should be at least 4 characters long and max of 25"
 * checks if name is of the correct format, else returns "name should not have at least 1 special character and at least 1 digit"
 * @param {} name
 * @returns
 */
 const _name_validator = (name) => {

  if (typeof name != "string") {
     return "name should be of string type";
  }

  if (name.length < 3) {
    return "name length should be at least 4 characters long";
  }

  if (name.length > 25) {
    return "name length shouldn't be 25 characters long";
  }

  if (!name.match(/^[a-zA-Z ]+$/)) {
    return "name should be of the correct format";
  }

  return "";
};

/**
 * Helper function to validate employee id as number
 * checks if empid is too short, else returns "email should be at least 3 digits and max 10 digits"
 * @param {} email
 * @returns
 */
const _empid_validator = (empid) => {
  // if (typeof empid !== "number") return "Employee id should be of type Number";
  empid = empid.toString();
  if (empid.length < 3) {
    return "Employee id should be have more then three digits";
  }
  if (empid.length > 10) {
    return "Employee id should be less than ten digits";
  }
  return "";
};

/**
 * Helper function to validate password string
 * checks if new_password is too short, else returns "new_password should be at least 10 characters long and max of 20"
 * checks if new_password is of the correct format, else returns "new_password should have at least 1 special character and at least 1 digit"
 * @param {} password
 * @returns
 */
const _password_validator = (password) => {
  password = password.toString();
  if (password.length < 6) {
    return "password length should be at least 6 characters long";
  }
  if (password.length > 15) {
    return "password length should be a max of 15";
  }
  if (
    !password.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,15}$/)
  ) {
    return "Password should have at least 1 digit, one special character ";
  }

  return "";
};

/**
 * Helper function to validate new_password string
 * checks if new_password is too short, else returns "new_password should be at least 10 characters long and max of 20"
 * checks if new_password is of the correct format, else returns "new_password should have at least 1 special character and at least 1 digit"
 * @param {} new_password
 * @returns
 */
const _new_password_validator = (new_password) => {
  if (new_password.length < 6) {
    return "new_password length should be at least 10 characters long";
  }
  if (new_password.length > 15) {
    return "new_password length should be a max of 15";
  }
  if (
    !new_password.match(
      /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,15}$/
    )
  ) {
    return "new_password should be of the correct format";
  }

  return "";
};

/*
 * Helper function to validate security_question string
 * checks if _security_question_validator is string, else returns "security_question should be of string"
 * @param {}security_question
 * @returns
 */
const _security_question_validator = (security_question) => {
  if (typeof security_question != "string") {
    return "security_question should be of string type";
  }
  return "";
};

/*
 * Helper function to validate security_answer string
 * checks if _security_answer_validator is string, else returns "security_answer should be of string"
 * @param {}security_answer
 * @returns
 */
const _security_answer_validator = (security_answer) => {
  if (typeof security_answer != "string") {
    return "security_answer should be of string type";
  }
  return "";
};

/*
 * Helper function to validate date string
 * checks if _date_validator  is string, else returns "date should be of string"
 * @param {} date
 * @returns
 */
const _date_validator = (date) => {
  if (typeof date != "string") {
    return "date should be of string type";
  }
  return "";
};

/*
 * Helper function to validate task_name string
 * checks if _taskname_validator  is string, else returns "task_name should be of string"
 * @param {}task_name
 * @returns
 */
const _taskname_validator = (task_name) => {
  if (typeof task_name != "string") {
    return "task_name should be of string type";
  }
  return "";
};

/*
 * Helper function to validate hours string
 * checks if _hours_validator  is string, else returns "hours should be of string"
 * @param {}hours
 * @returns
 */
const _hours_validator = (hours) => {
  if (typeof hours != "string") {
    return "hours should be of string type";
  }
  return "";
};

/*
 * Helper function to validate description string
 * checks if _description_validator  is string, else returns "description should be of string"
 * @param {}description
 * @returns
 */
const _description_validator = (description) => {
  if (typeof description != "string") {
    return "description should be of string type";
  }
  return "";
};

/**
 * Helper function to validate dateofjoining string
 * checks if dateofjoining is string, else returns "dateofjoining should be of string type"
 * checks if dateofjoining is too short, else returns "dateofjoining should be at least 6 characters long and max of 15"
 * checks if dateofjoining is of the correct format, else returns "dateofjoining should not have at least 1 special character"
 * @param {} dateofjoining
 * @returns
 */
const _date_of_joining_validator = (dateofjoining) => {
  if (typeof dateofjoining != "string") {
    return "dateofjoining should be of string type";
  }

  if (dateofjoining.length < 4) {
    return "dateofjoining length should be at least 6 characters long";
  }

  if (dateofjoining.length > 21) {
    return "dateofjoining length should have 8 characters long";
  }
  return "";
};

/**
 * Helper function to validate role
 * checks if role is string, else returns "role should be of string type"
 * checks if role is too short, else returns "role should be at least 1 characters long and max of 50"
 * checks if role is of the correct format, else returns "role should be of the correct format" 406
 * @param {} role
 * @returns
 */
const _role_validator = (role) => {
  if (typeof role != "string") {
    return "role should be of string type";
  }
  if (
    !(
      role.toUpperCase() === "ADMIN" ||
      role.toUpperCase() === "HR" ||
      role.toUpperCase() === "EMPLOYEE"
    )
  ) {
    return "Currently there are only three types of roles ADMIN/HR/EMPLOYEE";
  }
  return "";
};

/**
 * Helper function to validate email string
 * checks if email is string, else returns "email should be of string type"
 * checks if email is too short, else returns "email should be at least 6 characters long and max of 50"
 * checks if email is of the correct format, else returns "email should be of the correct format" 406
 * @param {} email
 * @returns
 */
const _email_validator = (email) => {
  // is it a string
  if (typeof email != "string") {
    return "email should be of string type";
  }
  if (email.length < 6) {
    return "email length should be at least 6 characters long";
  }
  if (email.length > 50) {
    return "email length should be a max of 50";
  }
  if (
    !email.match(
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
  ) {
    return "email should be of the correct format";
  }

  return "";
};

/**
 * Helper function to validate phone number
 * checks if phone no is number, else returns "phone number should be of number type"
 * checks if phone no is too short, else returns "phone number should be at least 1 characters long and max of 10"
 * checks if phone no is of the correct format, else returns "phone number should be of the correct format" 406
 * @param {} phoneNo
 * @returns
 */
const _phoneNo_validator = (phoneNo) => {
  // if (typeof phoneNo != "number") {
    // return "phone number should be of number type";
  // }

  if (phoneNo.toString().length < 10) {
    return "phone number length should be at least 10 characters long";
  }

  if (phoneNo.length > 10) {
    return "phone number length should be a max of 10";
  }
  return "";
};


/**
 * Helper function to validate address
 * it checks wether the address is in string type or not.
 * @param {} address
 * @returns
 */
const _address_validator = (description) => {
  if (typeof description != "string") {
    return "description should be of string type";
  }
  return "";
};


export {
  _name_validator,
  _empid_validator,
  _password_validator,
  _new_password_validator,
  _security_question_validator,
  _security_answer_validator,
  _taskname_validator,
  _date_validator,
  _hours_validator,
  _description_validator,
  _date_of_joining_validator,
  _role_validator,
  _phoneNo_validator,
  _email_validator,
  _address_validator
};
