//imported external dependencies
import React, { Component } from "react";

//imported CSS for styling the page
import "../Timesheet/addtask.css";

//class component for Timesheet page
class AddTask extends Component {
  //render is used to display the specified HTML code inside the specified HTML element.
  render() {
    //return is used to dislay the object after rendering is done
    return (
        <>
        <div id="addtask" class="modal fade" role="dialog">
          <div class="modal-dialog">
            {/* <!-- Modal content--> */}
            <div class="modal-content timesheet">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                  &times;
                </button>
                <h4 class="modal-title">Add Task</h4>
              </div>
              <div class="modal-body">
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-3">
                          <p>Project</p>
                        </div>
                        <div class="col-md-9">
                          <select class="js-example-basic-single">
                            <option>Project1</option>
                            <option>Project2</option>
                            <option>Project3</option>
                            <option>Project4</option>
                            <option>Project5</option>
                          </select>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">
                          <p>Task</p>
                        </div>
                        <div class="col-md-9">
                          <select class="js-example-basic-single">
                            <option>Task1</option>
                            <option>Task2</option>
                            <option>Task3</option>
                            <option>Task4</option>
                            <option>Task5</option>
                          </select>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">
                          <p>Date</p>
                        </div>
                        <div class="col-md-9">
                          <input type="date" />
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">
                          <p>Start Time</p>
                        </div>
                        <div class="col-md-9">
                          <input type="time" />
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">
                          <p>End Time</p>
                        </div>
                        <div class="col-md-9">
                          <input type="time" />
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">
                          <p>Description</p>
                        </div>
                        <div class="col-md-9">
                          <input type="text" class="timesheet-description" />
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                          <div clas="actionbutton">
                            <p class="button-container">
                              <button class="user-aciton">Add Task</button>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div />
        </>
    );
  }
}

export {AddTask};