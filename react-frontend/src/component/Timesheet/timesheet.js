//imported external dependencies
import React, { Component } from "react";

//imported CSS for styling the page
import "../Timesheet/timesheet.css";
import { Link } from "react-router-dom";
import "../Timesheet/addtask.js";

//class component for Timesheet page
class Timesheet extends Component {
  //render is used to display the specified HTML code inside the specified HTML element.
  render() {
    //return is used to dislay the object after rendering is done
    return (
      <>
        <div>
          <p class="maintitle">Monthly Timesheet</p>
        </div>
        <section class="timesheet-navigation" />
        <div class="nav" />
        <div class="container-fluid nopaddingmail" />
        <div class="table" />
        <ul class="nav nav-tabs" data-tabs="tabs" id="myTab">
          <li class="active">
            <a data-toggle="tab" href="#incoming">
              Current
            </a>
          </li>
          <li>
            <a data-toggle="tab" href="#sentmsg">
              Previous
            </a>
          </li>
          <li>
            <a data-toggle="tab" href="#sentmsg">
              Accepted
            </a>
          </li>
          <li>
            <a data-toggle="tab" href="#sentmsg">
              Rejected
            </a>
          </li>
        </ul>
        <div class="tab-content" />
        <div class="tab-pane active" id="incoming"></div>
        <section />
        <section class="timesheet-buttons">
          <input type="date" id="theDate" />

          <div class="today-timesheet">
            <button type="button" class="newmsgb">
              Today
            </button>
            <Link to="/AddTask">
            <button
              type="button"
              class="add-task-timesheet"
              data-toggle="modal"
              data-target="#addtask"
            >
              Add New Task
            </button>
            </Link>
          </div>
        </section>
        <section>
          <p class="picked-day">2016-12-30</p>
        </section>
        <section>
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 tab-title">
                <div class="row">
                  <div class="col-md-2 col-sm-2 col-xs-1">
                    <div class="statustitle">Project</div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">
                    <div class="projectnametitle">Task</div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">
                    <div class="completiontitle">Date</div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">
                    <div class="detailstitle">Start Time/End Time</div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-1">
                    <div class="detailstitle">Duration</div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">
                    <div class="detailstitle">Description</div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-1">
                    <div class="tsdelete-row"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row timesheet-task-row">
                  <div class="col-md-2 col-sm-2 col-xs-1">
                    <div class="statustitle">Project 1</div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">
                    <div class="projectnametitle">Task 1</div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">
                    <div class="completiontitle">2016-12-12</div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">
                    <div class="detailstitle">12:00/13:00</div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-1">
                    <div class="detailstitle">1 Hr.</div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-2">
                    <div class="detailstitle">Really hard work.</div>
                  </div>
                  <div class="col-md-1 col-sm-1 col-xs-1">
                    <div class="tsdelete-row">x</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        
      </>
    );
  }
}


//exporting timesheet module
export default Timesheet;
