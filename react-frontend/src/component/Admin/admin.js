import { Component } from 'react';
//importing internal modules 
import AllUsers from '../Admin/AllUsers';
import AddUser from './AddUser/AddUser';
import EditUser from '../Admin/EditUser';
import NavBar from '../Admin/navbar';
import AdminHome from '../Admin/admin_home';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
//import AddEmployee from "../Admin/register";

class Admin extends Component {
  render(){
  return (
    <BrowserRouter>
      <NavBar />
      <Switch>
      <Route exact path="/Admin" component={AdminHome} />
      <Route exact path="/" component={AdminHome}/>
      <Route exact path="/all" component={AllUsers} />
      <Route exact path="/add" component={AddUser} />
      <Route exact path="/edit/:id" component={EditUser} />
      </Switch>
    </BrowserRouter>
    //<AddEmployee></AddEmployee>
  );
}
}

//exporting Admin module
export default Admin;