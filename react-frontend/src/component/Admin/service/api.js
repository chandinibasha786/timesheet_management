import axios from 'axios';

const usersUrl = 'http://localhost:3001/users';


export const getUsers = async (empid) => {
    empid = empid || '';
    return await axios.get(`${usersUrl}/${empid}`);
}

export const addUser = async (user) => {
    return await axios.post(`${usersUrl}/add`, user);
}

export const deleteUser = async (empid) => {
    return await axios.delete(`${usersUrl}/${empid}`);
}

export const editUser = async (empid, user) => {
    return await axios.put(`${usersUrl}/${empid}`, user)
}