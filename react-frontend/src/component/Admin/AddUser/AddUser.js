//importing external dependencies 
import React, { useState } from "react";
import { useHistory } from "react-router-dom";

//importing css for styling purpose 
import "../../Admin/AddUser/AddUser.css";

//importing param validators
import {
  _name_validator,
  _empid_validator,
  _password_validator,
  _date_of_joining_validator,
  _role_validator,
  _email_validator,
  _security_question_validator,
  _security_answer_validator,
  _phoneNo_validator,
  _address_validator
} from "../../../../src/react-validators/param_validators";


//function  to addemp using validators 

function AddEmp() {
  const history = { useHistory };

  const [name, setName] = useState("");
  const [empid, setEmpid] = useState("");
  const [password, setPassword] = useState("");
  const [dateofjoining, setDateOfJoining] = useState("");
  const [role, setRole] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNo, setPhoneNo] = useState("");
  const [security_question, setSecurity_question] = useState("");
  const [security_answer, setSecurity_answer] = useState("");
  const [address, setAddress] = useState("");
  const [headStatus, setHeadStatus] = useState("");

  const [nameError, setNameError] = useState("");
  const [empidError, setEmpidError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [dateofjoiningError, setDateOfJoiningError] = useState("");
  const [roleError, setRoleError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [phoneNoError, setPhoneNoError] = useState("");
  const [addressError, setAddressError] = useState("");
  const [security_questionError, setSecurity_questionError] = useState("");
  const [security_answerError, setSecurity_answerError] = useState("");

  // Name
  const onChangeName = (event) => {
    setName(event.target.value);
    setNameError("");
  };

  const onBlurName = (event) => {
    validateName(event.target.value);
  };
  const validateName = (name) => {
    setNameError(_name_validator(name));
  };

  //employee ID
  const onChangeEmpid = (event) => {
    setEmpid(event.target.value);
    setEmpidError("");
  };

  const onBlurEmpid = (event) => {
    validateEmpid(event.target.value);
    
  };

  const validateEmpid = (empid) => {
    setEmpidError(_empid_validator(empid));
  };

  //password
  const onChangePassword = (event) => {
    setPassword(event.target.value);
    setPasswordError("");
  };

  const onBlurPassword = (event) => {
    validatePassword(event.target.value);
  };
  const validatePassword = (password) => {
    setPasswordError(_password_validator(password));
  };

  //date of joining
  const onChangeDateOfJoining = (event) => {
    setDateOfJoining(event.target.value);
    setDateOfJoiningError("");
  };

  const onBlurDateOfJoining = (event) => {
    validateDateOfJoining(event.target.value);
  };
  const validateDateOfJoining = (dateofjoining) => {
    setDateOfJoiningError(_date_of_joining_validator(dateofjoining));
  };

  //Role
  const onChangeRole = (event) => {
    setRole(event.target.value);
    setRoleError("");
  };

  const onBlurRole = (event) => {
    validateRole(event.target.value);
  };
  const validateRole = (role) => {
    setRoleError(_role_validator(role));
  };

  //Email
  const onChangeEmail = (event) => {
    setEmail(event.target.value);
    setEmailError("");
  };

  const onBlurEmail = (event) => {
    validateEmail(event.target.value);
  };
  const validateEmail = (email) => {
    setEmailError(_email_validator(email));
  };

  //Phone number
  const onChangePhoneNo = (event) => {
    setPhoneNo(parseInt(event.target.value));
    setPhoneNoError("");
  };

  const onBlurPhoneNo = (event) => {
    validatePhoneNo(event.target.value);
  };
  const validatePhoneNo = (phoneNo) => {
    setPhoneNoError(_phoneNo_validator(phoneNo));
  };
  //security question
  const onChangeSecurityQuestion = (event) => {
    setSecurity_question(event.target.value);
    setSecurity_questionError("");
  };

  const onBlurSecurityQuestion = (event) => {
    validateSecurityQuestion(event.target.value);
  };
  const validateSecurityQuestion = (security_question) => {
    setSecurity_questionError(_security_question_validator(security_question));
  };

  //security answer
  const onChangeSecurityAnswer = (event) => {
    setSecurity_answer(event.target.value);
    setSecurity_answerError("");
  };

  const onBlurSecurityAnswer = (event) => {
    validateSecurityAnswer(event.target.value);
  };
  const validateSecurityAnswer = (security_answer) => {
    setSecurity_answerError(_security_answer_validator(security_answer));
  };

  //address
  const onChangeAddress = (event) => {
    setAddress(event.target.value);
    setAddressError("");
  };

  const onBlurAddress = (event) => {
    validateAddress(event.target.value);
  };
  const validateAddress = (address) => {
    setAddressError(_address_validator(address));
  };

  const resetErrors = () => {
    setNameError("");
    setEmpidError("");
    setPasswordError("");
    setDateOfJoiningError("");
    setRoleError("");
    setEmailError("");
    setPhoneNoError("");
    setSecurity_questionError("");
    setSecurity_answerError("");
    setAddressError("")
  };

  const resetFields = () => {
    setName("");
    setEmpid("");
    setPassword("");
    setDateOfJoining("");
    setRole("");
    setEmail("");
    setPhoneNo("");
    setSecurity_question("");
    setSecurity_answer("");
    setAddress("")
  };

  //time out function
  const clearStatus = () => {
    setHeadStatus("");
  };
  //submitting the form
  const submitForm = async (event) => {
    event.preventDefault();
    resetErrors();
    validateName(name);
    validateEmpid(empid);
    validatePassword(password);
    validateDateOfJoining(dateofjoining);
    validateRole(role);
    validateEmail(email);
    validatePhoneNo(phoneNo);
    validateSecurityQuestion(security_question);
    validateSecurityAnswer(security_answer);
    validateAddress(address)

    if (
      _name_validator(name).length > 0 ||
      _empid_validator(empid).length > 0 ||
      _password_validator(password).length > 0 ||
      _date_of_joining_validator(dateofjoining).length > 0 ||
      _role_validator(role).length > 0 ||
      _email_validator(email).length > 0 ||
      _phoneNo_validator(phoneNo).length > 0 ||
      _security_question_validator(security_question).length > 0 ||
      _security_answer_validator(security_answer).length > 0 ||
      _address_validator(address).length >0    
      ) {
      console.log("ERROR");
      return;
    } else {
      // const { empid, new_password, security_question, security_answer } = this.state;
      const userDetails = {
        name,
        empid,
        password,
        dateofjoining,
        role,
        email,
        phoneNo,
        security_question,
        security_answer,
        address,
      };
      const url = "http://localhost:3001/post_user_profile";
      const options = {
        method: "POST",
        body: JSON.stringify(userDetails),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          authorization:
            "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5hdmVlbkBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NDY5Nzg3Nzh9.Bz5nf7tmJld7qwYXtSdtRNxx0_PFAeDE0qZOwvzmHL4",
        },
      };

      try {
        const response = await fetch(url, options);
        //console.log(response.status)
        const data = await response.json();
        console.log("data :", data);
        if (data.msg === "Profile created successfully") {
          setHeadStatus(data.msg);
          setTimeout(clearStatus, 4000);
          resetFields();
        } else {
          setHeadStatus(data.msg);
          setTimeout(clearStatus, 5000);
          console.log(resetFields);
        }
      } catch (error) {
        // unknown error
        setHeadStatus("Unable to contact server.");
      }
    }
  };
  
  //returns the html page with the above validations to add a new user into DB
  return (
    <div className="container_admin">
      <div className="wrapper_admin">
        <form>
        <h1 className="headstatus" style={{color:"red", fontSize:"14px"}}>{headStatus}</h1>
          <input
            type="text"
            id="name"
            name="name"
            value={name}
            onChange={(event) => onChangeName(event)}
            onBlur={(event) => onBlurName(event)}
            required
            placeholder="Full Name"
          />
          <div>{nameError}</div>
          <input
            type="text"
            id="employee"
            name="EmpId"
            value={empid}
            onChange={(event) => onChangeEmpid(event)}
            onBlur={(event) => onBlurEmpid(event)}
            required
            placeholder="Employee Id"
          />
          <div>{empidError}</div>
          <input
            type="text"
            id="role"
            name="role"
            value={role}
            onChange={(event) => onChangeRole(event)}
            onBlur={(event) => onBlurRole(event)}
            required
            placeholder="Role"
          />
          <div>{roleError}</div>
          <input
            type="text"
            id="address"
            name="address"
            value={address}
            onChange={(event) => onChangeAddress(event)}
            onBlur={(event) => onBlurAddress(event)}
            required
            placeholder="Address"
          />
          <div>{addressError}</div>
          <input
            type="email"
            id="email"
            name="Email"
            value={email}
            onChange={(event) => onChangeEmail(event)}
            onBlur={(event) => onBlurEmail(event)}
            required
            placeholder="Email"
          />
          <div>{emailError}</div>
          <input
            type="number"
            id="phoneno"
            name="PhoneNo"
            value={phoneNo}
            onChange={(event) => onChangePhoneNo(event)}
            onBlur={(event) => onBlurPhoneNo(event)}
            required
            placeholder="Phone Number"
          />
          <div>{phoneNoError}</div>
          <input
            type="password"
            id="password_"
            name="EmpId"
            value={password}
            onChange={(event) => onChangePassword(event)}
            onBlur={(event) => onBlurPassword(event)}
            required
            placeholder="Password"
          />
          <div>{passwordError}</div>
          <input
            type="text"
            id="securityques"
            name="security_question"
            value={security_question}
            onChange={(event) => onChangeSecurityQuestion(event)}
            onBlur={(event) => onBlurSecurityQuestion(event)}
            required
            placeholder="Security Question"
          />
          <div>{security_questionError}</div>
          <input
            type="password"
            id="securityans"
            name="security_answer"
            value={security_answer}
            onChange={(event) => onChangeSecurityAnswer(event)}
            onBlur={(event) => onBlurSecurityAnswer(event)}
            required
            placeholder="Security Answer"
          />
          <div>{security_answerError}</div>
          <input
            type="number"
            id="dateofjoining"
            name="dateofjoining"
            value={dateofjoining}
            onChange={(event) => onChangeDateOfJoining(event)}
            onBlur={(event) => onBlurDateOfJoining(event)}
            required
            placeholder="Date of Joining  DDMMYYYY"
          />
          <div>{dateofjoiningError}</div>
          <input
            type="submit"
            id="login-btn"
            value="Add"
            onClick={submitForm}
          />
        </form>
      </div>
    </div>
  );
}

//exporting AddEmp module
export default AddEmp;
