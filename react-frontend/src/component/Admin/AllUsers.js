//importing external modules
import { useState, useEffect } from 'react';
import { Table, TableHead, TableCell, TableRow, TableBody, Button, makeStyles } from '@material-ui/core'
import { getUsers, deleteUser, editUser } from '../Admin/service/api';
import { Link } from 'react-router-dom';
import { backend_endpoint } from "../../constants.js";

//using styles for the page
const useStyles = makeStyles({
    table: {
        width: '100%',
        margin: '0',
        overflow: 'scroll', 
        background: '#ffffff'
    },
    thead: {
        '& > *': {
            fontSize: 20,
            background: '#000000',
            color: '#FFFFFF'
        }
    },
    row: {
        '& > *': {
            fontSize: 14,
            padding: '3px'
        }
    }
})

const AllUsers = () => {
    const [users, setUsers] = useState([]);
    const classes = useStyles();

    useEffect(() => {
        getAllUsers();
    }, []);

    const deleteUserData = async (empid) => {
        await deleteUser(empid);
        getAllUsers();
    }

    const editUserData = async (empid) => {
        await editUser(empid);
        getAllUsers();
    }

    const getAllUsers = async () => {
        let response = await getUsers();
        setUsers(response.data);
        fetch(backend_endpoint + "/users")
    }

    return (
        <Table className={classes.table} data-spy="scroll">
            <TableHead>
                <TableRow className={classes.thead}>
                    <TableCell>Employee_Id</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Date of Joining</TableCell>
                    <TableCell>Role</TableCell>
                    <TableCell>Phone</TableCell>
                    <TableCell></TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {users.map((user) => (
                    <TableRow className={classes.row} key={user._empid}>
                        <TableCell>{user.empid}</TableCell>
                        <TableCell>{user.email}</TableCell>
                        <TableCell>{user.dateofjoining}</TableCell>
                        <TableCell>{user.role}</TableCell>
                        <TableCell>{user.phoneNo}</TableCell>
                        <TableCell>
                            <Button variant="contained" style={{marginRight:10}} onClick={() => editUserData(user._empid)} component={Link} to={`/edit/${user._empid}`}>Edit</Button>
                            <Button variant="contained" onClick={() => deleteUserData(user._empid)}>Delete</Button> 
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    )
}

//exporting allusers module
export default AllUsers;
