//importing external ependencies
import { useState, useEffect } from 'react';
import { FormGroup, Button, makeStyles } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { getUsers, editUser } from '../Admin/service/api';

//initilizing the keys for validations 
const initialValue = {
    name: '',
    email: '',
    address: '',
    empid: '',
    dateofjoining: '',
    role: '',
    phoneNo: '',
    security_question: '',
    security_answer: '',
    password: '',


}

//using styles for the page
const useStyles = makeStyles({
    container: {
        width: '100%',
        margin: '0',
        '& > *': {
            marginTop: 0
        }
    }
    
})

//admin can edit the user data based on the following params 
const EditUser = () => {
    const [user, setUser] = useState(initialValue);
    const { name, email, address, empid, password, dateofjoining, role, phoneNo, security_question, security_answer } = user;
    const classes = useStyles();
    let history = useHistory();

    useEffect(() => {
        loadUserDetails();
    }, []);

    const loadUserDetails = async() => {
        const response = await getUsers(empid);
        setUser(response.data);
    }

    const editUserDetails = async() => {
        const response = await editUser(empid);
        history.push('/all');
        setUser(response.data);
    }

    // const onValueChange = (e) => {
    //     console.log(e.target.value);
    //     setUser({...user, [e.target.name]: e.target.value})
    // }
    //returns an html page based on the above validations 
    return (
        <FormGroup className={classes.container}>
        <div className="container_admin">
      <div className="wrapper_admin">
        <form>
          <input
            type="text"
            id="name"
            name="name"
            value={name}
            required
            placeholder="Full Name"
          />

          <input
            type="text"
            id="employee"
            name="EmpId"
            value={empid}
            required
            placeholder="Employee Id"
          />

          <input
            type="text"
            id="role"
            name="role"
            value={role}
            required
            placeholder="Role"
          />

          <input
            type="text"
            id="address"
            name="address"
            value={address}
            required
            placeholder="Address"
          />

          <input
            type="email"
            id="email"
            name="Email"
            value={email}
            required
            placeholder="Email"
          />
          <input
            type="number"
            id="phoneno"
            name="PhoneNo"
            value={phoneNo}
            required
            placeholder="Phone Number"
          />
          <input
            type="password"
            id="password_"
            name="EmpId"
            value={password}
            required
            placeholder="Password"
          />

          <input
            type="text"
            id="securityques"
            name="security_question"
            value={security_question}
            required
            placeholder="Security Question"
          />
          <input
            type="password"
            id="securityans"
            name="security_answer"
            value={security_answer}
            required
            placeholder="Security Answer"
          />
          <input
            type="number"
            id="dateofjoining"
            name="dateofjoining"
            value={dateofjoining}
            required
            placeholder="Date of Joining  DDMMYYYY"
          />
          </form>
         
          
          
                <Button style={{backgroundColor:"green", 
                position:"absolute", paddingLeft:"50px", 
                marginTop:"30%", fontSize:"20px"}} 
                onClick={() => editUserDetails()}>Edit User</Button>
                </div>
                </div>
                
                
                
        </FormGroup>
    )
}

//exporting edituser module
export default EditUser;