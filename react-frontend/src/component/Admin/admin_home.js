//importing external dependencies
import { Box, Typography, makeStyles } from '@material-ui/core';
//importing images for the admin page
import Youtube from '../Admin/Images/admin_homes.jpg';

//styling for the page to be loaded 
const useStyles = makeStyles({
    component: {
        margin: 50,
        '& > *': {
            marginTop: 50
        }
    },
    image: {
        width: '50%',
        height: '50%'
    }
})

/**
 * function for admin to redirect to home page of admin.
 * @returns a page where the admin can see what the works need to be done.
 */
const AdminHome = () => {
    const classes = useStyles();

    return (
        <Box className={classes.component}>
            <Typography variant="h4">Admin Home</Typography>
            <Box style={{display: 'flex'}}>
                <img src={Youtube} className={classes.image} alt={Youtube} />
            </Box>
        </Box>
    )
}

//exporting adminhome module 
export default AdminHome;