//importing external dependencies
import React, { Component } from "react";
import { Link } from "react-router-dom";

//importing port from constants
import { backend_endpoint } from "../../constants.js";
import Cookies from "js-cookie";

//importing css for styling
import "./login.css";
//import { useSelector, useDispatch } from "react-redux";
import { _email_validator, _password_validator } from "../../react-validators/param_validators";
//import { setLoginData } from "../../Redux/Actions/actions.js"

//class component for login
class Login extends Component {
	//using Constructor function for Props 
	constructor(props) {
    super(props);
    this.state = {
			email: "",
			password: "",
			email_err: "",
			password_err: ""
		};
  }

//validations for login by email and password 

  onChangeEmail = (event) => {
		this.setState({ email_err: ""});
		this.setState({ email: event.target.value });
	};
	onBlurEmail = (event) => {
		this.validateEmail(event.target.value);
	}
	validateEmail = (email) => {
		this.setState({ email_err: _email_validator(email) });
	}


  onChangePassword = (event) => {
		this.setState({ password_err: ""});
    this.setState({ password: event.target.value });
  };
	onBlurPassword = (event) => {
		this.validatePassword(event.target.value);
	}
	validatePassword = (password) => {
		this.setState({ password_err: _password_validator(password) });
	}

	resetErrors = () => {
		this.setState({ email_err: "", password_err: "" })
	}

  submitForm = async (event) => {

		event.preventDefault();
    
		this.resetErrors();
		this.validateEmail(this.state.email);
		this.validatePassword(this.state.password);

		if (_email_validator(this.state.email).length > 0 || _password_validator(this.state.password).length > 0) {
			return;
		} else {
			const { email, password } = this.state;
			const userDetails = { email, password };
			const url = backend_endpoint + "/login";
			const options = {
				method: "POST",
				body: JSON.stringify(userDetails),
				headers: {
					"Content-Type": "application/json",
					"Accept": "application/json",
					"Authentication" : "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im5hdmVlbkBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE2NDY5Nzg3Nzh9.Bz5nf7tmJld7qwYXtSdtRNxx0_PFAeDE0qZOwvzmHL4"
				},
			};
	
			try {
				const response = await fetch(url, options);
				const data = await response.json();

				if (response.status === 200) {
					if (data.jwt_token) {
						//this.dispatch(setLoginData(data))
						const { history } = this.props;
						Cookies.set("jwt_token", data.jwt_token, {
							expires: 30,
						});
						history.replace("/home");
					} else {
						// no jwt token recieved
						this.setState({ password_err: "No JWT Token recieved"})
					}
				} else {
					this.setState({ password_err: "User doesn't exists" })
				}
			} catch (error) {
				// unknown error
				this.setState({ password_err: "Unable to contact server." })
			}
		}
  };

  render() {
    const { password, email } = this.state;
		//returns an html document for the USER to login based on the above validations 
    return (
      <div className="login_container">
        <div className="login_form_container">
          <div className="left">
            <form className="form_container_login">
              <h1>Login</h1>
              <input type="email"  className="input_email" placeholder="Email"
                onChange={this.onChangeEmail}  onBlur={this.onBlurEmail} value={email} />
							<p style={{color: "red", fontSize: "x-small", textAlign: "center"}}>{this.state.email_err}</p>
              <input type="password" className="input" placeholder="Password"
								onChange={this.onChangePassword} onBlur={this.onBlurPassword} value={password} />
							<p style={{color: "red", fontSize: "x-small", textAlign: "center"}}>{this.state.password_err}</p>
							<button type="submit" className="green_btn" onClick={this.submitForm}>Sign In</button>
						</form></div>
          <div className="right">
					<Link to="/Forgot">
						<button type="button" className="forgot_button">
							Forgot Password
						</button>
					</Link></div></div></div>);}
}

//exporting login module
export  {Login};
