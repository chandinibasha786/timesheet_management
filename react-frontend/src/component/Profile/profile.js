//imported external dependencies
import React, { Component } from "react";
import { Link } from 'react-router-dom';
//imported CSS for styling the page
import "../Profile/profile.css";

//class component for Timesheet page
class Profile extends Component {
  //render is used to display the specified HTML code inside the specified HTML element.
  render() {
    //return is used to dislay the object after rendering is done
    return (
      <>
        <div class="navbar-top">
          <div class="title">
            <h1>Profile</h1>
          </div>
        </div>
        {/* <!-- End --> */}

        {/* <!-- Sidenav --> */}
        <div class="sidenav">
          <div class="profile">
            <img
              src="employee-Deepak Sethi.jpg"
              alt="profile_pic"
              width="100"
              height="100"
            />

            <div class="name">Naveen Purimetla</div>
            <div class="job">UI Developer</div>
          </div>

          <div class="sidenav-url">
            <div class="url">
              <a href="#profile" class="active">
                Profile
              </a>
              <hr align="center" />
            </div>
            
            <div class="url">
            <Link to="/">
              <a href="#profile" class="active">
                Logout
              </a>
              
              <hr align="center" />
              </Link>
            </div>
          </div>
        </div>
        {/* <!-- End --> */}

        {/* <!-- Main --> */}
        <div class="main">
          <h2>IDENTITY</h2>
          <div class="card">
            <div class="card-body">
              <i class="fa fa-pen fa-xs edit"></i>
              <table>
                <tbody>
                  <tr>
                    <td>Name</td>
                    <td>:</td>
                    <td>Naveen Purimetla</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>naveenp@gmail.com</td>
                  </tr>
                  <tr>
                    <td>Address</td>
                    <td>:</td>
                    <td>Hyderabad</td>
                  </tr>
                  <tr>
                    <td>Employee Id</td>
                    <td>:</td>
                    <td>10775</td>
                  </tr>
                  <tr>
                    <td>Role</td>
                    <td>:</td>
                    <td>Employee</td>
                  </tr>
                  <tr>
                    <td>Date of Joining</td>
                    <td>:</td>
                    <td>11-01-2022</td>
                  </tr>
                  <tr>
                    <td>Phone Number</td>
                    <td>:</td>
                    <td>9876564567</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </>
    );
  }
}

//exporting Profile module
export default Profile;
