//importing external dependencies
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { backend_endpoint } from "../../constants.js";
//importing css file for styling
import './forgot.css';
//importing internal validators 
import { _email_validator, _security_answer_validator,_new_password_validator } from "../../react-validators/param_validators.js";


/* in login form ,taking the username and newPassword as a string
  *and showing error if user does not fill the details and not following some conditions 
*/
class Forgot extends Component {
  constructor(props) {
    super(props);
    this.state = {
			email: "",
			new_password: "",
      security_answer:"",
			email_err: "",
			new_password_err: "",
      security_answer_err:""
		};
  }

  onChangeEmail = (event) => {
		this.setState({ email_err: ""});
    this.setState({ email: event.target.value });
  };
	onBlurEmail = (event) => {
		this.validateEmail(event.target.value);
	}
	validateEmail = (email) => {
		this.setState({ email_err: _email_validator(email) });
	}


  onChangeNewPassword = (event) => {
		this.setState({ new_password_err: ""});
    this.setState({ new_password: event.target.value });
  };
	onBlurNewPassword = (event) => {
		this.validatePassword(event.target.value);
	}
	validateNewPassword = (new_password) => {
		this.setState({ new_password_err: _new_password_validator(new_password) });
	}

  onChangeSecurityAnswer = (event) => {
		this.setState({ security_answer_err: ""});
    this.setState({ security_answer: event.target.value });
  };
	onBlurSecurityAnswer = (event) => {
		this.validateSecurityAnswer(event.target.value);
	}
	validateSecurityAnswer = (security_answer) => {
		this.setState({ security_answer_err: _security_answer_validator(security_answer) });
	}

	resetErrors = () => {
		this.setState({ email_err: "", new_password_err: "", security_answer_err:"" })
	}

  submitForm = async (event) => {
		event.preventDefault();
    this.resetErrors();
		this.validateEmail(this.state.email);
		this.validateNewPassword(this.state.new_password);
    this.validateSecurityAnswer(this.state.security_answer);
  

		if (_email_validator(this.state.email).length > 0 || _new_password_validator(this.state.new_password).length > 0 ||_security_answer_validator(this.state.security_answer).length > 0 ) {
			return;
		} else {
			const { email, new_password, security_answer } = this.state;
			const userDetails = { email, new_password,security_answer };
			const url = backend_endpoint + "/forgot_password";
			const options = {
				method: "PUT",
				body: JSON.stringify(userDetails),
				headers: {
					"Content-Type": "application/json",
					"Accept": "application/json",
				},
			};
	
			try {
				const response = await fetch(url, options);
				//console.log(response.status)
				const data = await response.json();
				//console.log(data)

				if (response.status === 200) {
					alert("updated succesfully")
					//console.log("hello")
					if (data) {
						const { history } = this.props;
						history.replace("/home");
					} else {
						// no jwt token recieved
						this.setState({ new_password_err: "No data recieved"})
					}
				} else {
					this.setState({ new_password_err: "User doesn't exists" })
				}
			} catch (error) {
				// unknown error
				this.setState({ new_password_err: "Unable to contact server." })
			}}}
//renders email,new_password,security_answer
	render(){
		const {email, new_password,security_answer} = this.state

	return (
      <div>
        <header>
            <title>Forgot Password Form</title>
        </header>
        <body>
        <h1>Forgot your password?</h1>
				<form onSubmit={this.submitForm}>
          <input type="email_forgot" id='email_forgot' name="email" placeholder="Enter your email address" required 
					 onChange={this.onChangeEmail}  onBlur={this.onBlurEmail} value={email}/>
					<p style={{color: "red", fontSize: "x-small", textAlign: "center"}}>{this.state.email_err}</p>

					<input type="security_answer" id='security_answer' name="security_answer" placeholder="Enter your Security Answer" required 
					onChange={this.onChangeSecurityAnswer} onBlur={this.onBlurSecurityAnswer} value={security_answer}/>
          <p style={{color: "red", fontSize: "x-small", textAlign: "center"}}>{this.state.security_answer_err}</p>
					
					<input type="New_Password" id="New_Password" name="New_Password" placeholder="Enter your New Password" required 
					onChange={this.onChangeNewPassword} onBlur={this.onBlurNewPassword} value={new_password}/>  
					<p style={{color: "red", fontSize: "x-small", textAlign: "center"}}>{this.state.new_password_err}</p>
        
				<button type="submit" id="submit_forgot">Submit</button>
        </form>
     		</body>
      </div>

	);
}
}

//exporting forgot module
export default withRouter(Forgot);