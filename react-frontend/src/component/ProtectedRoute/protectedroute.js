//importing external dependencies
import { Redirect,Route } from "react-router-dom";
import Cookies from "js-cookie";


const ProtectedRoute = props => {
  // const [user, setUser] = useContext(store)
  // const token = user[0].jwt_token
  // console.log(user);
  const token = Cookies.get('jwt_token')
  if (token === undefined){
    return <Redirect to = "/"/>
  }
  return <Route {...props}/>
}

//exporting ProtectedRoute module
export default ProtectedRoute;