//imported external dependencies
import React, { Component } from "react";
import { Link } from 'react-router-dom';
import Cookies from 'js-cookie'

//imported CSS for styling the page 
import "../Home/home.css";

//class component for Home page 
class Home extends Component {
  //render is used to display the specified HTML code inside the specified HTML element.
  logoutSession = ()=>{
    const { history } = this.props;
    Cookies.remove("jwt_token")
    const token = Cookies.get("jwt_token")
    if(token === undefined){
      history.replace("/")
    }
  }
  render() {
    //return is used to dislay the object after rendering is done
    return (
      <>
        <div id="move">
          <div id="circle">
            <p>ASPIRE</p>
          </div>
          <div id="main_home">
            <h1>Where you can brush up your skills</h1>
          </div>
        </div>
        <section id="options">
          <Link to='/Profile'>
          <a id="a2" href="../HTML/profile.html">
            Profile
          </a>
          </Link>
          <Link to='/Timesheet'>
          <a id="a3" href="../HTML/timesheet.html">
            Timesheet
          </a>
          </Link>
          <Link to='/Admin'>
          <a id="a4" href="../HTML/admin.html">
            Admin
          </a>
          </Link>
          <Link to='/Hr'>
          <a id="a5" href="../HTML/hr.html">
            HR Login
          </a>
          </Link>
          <Link to='/'>
          <a id="a1" href="../HTML/login.html" onClick={this.logoutSession}>
            Logout
            
          </a>
          </Link>
        </section>
      </>
    );
  }
}


//exporting Home module
export default Home;
