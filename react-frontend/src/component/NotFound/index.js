//imported CSS file for styling
import "./index.css";
import React, { Component } from "react";

//function for showing NOTFOUND content when an invalid path is given in the URL
class NotFound extends Component {
  render() {
    return (
      <div className="not-found-container">
        <img
          src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqI3lHFjBbLelg5rGnkZVukHUI2cd9cnEGOQ&usqp=CAU"
          alt="not found"
          className="not-found-img"
        />
      </div>
    );
  }
}

//exporting notfound module
export default NotFound;
