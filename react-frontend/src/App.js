import './App.css';
import {Login} from '../src/component/Login/login';
import Home from '../src/component/Home/home';
import Profile from '../src/component/Profile/profile';
import Timesheet from './component/Timesheet/timesheet';
import Admin from '../src/component/Admin/admin';
import Hr from '../src/component/Hr/hr.js';
import Forgot from './component/Forgot/forgot';
import { AddTask } from './component/Timesheet/addtask';
import ProtectedRoute from '../src/component/ProtectedRoute/protectedroute.js' 
import NotFound from '../src/component/NotFound/index.js';
import { BrowserRouter,Route,Switch } from "react-router-dom";
// import { createContext, useState } from 'react';

// export const store = createContext()
  
function App() {

  // const [name, setName] = useState([])
  return (
    <>
    {/* <store.Provider value={[name, setName]}> */}
    <BrowserRouter>
    <Switch>
      
      <Route exact path="/" component={Login}/>
      <ProtectedRoute exact  path="/Home" component={Home}/>
      <ProtectedRoute exact  path="/Profile" component={Profile}/>
      <ProtectedRoute exact path="/Timesheet" component={Timesheet}/>
      <ProtectedRoute exact path="/Admin" component={Admin}/>
      <ProtectedRoute exact path="/Hr" component={Hr}/>
      <Route path="/not-found" component={NotFound} />
      <Route exact path="/Forgot" component={Forgot}/>
      <Route path="/AddTask" component={AddTask}/>
      
    </Switch>
    </BrowserRouter>
    {/* </store.Provider> */}
    </>
  );
}

export default App;
