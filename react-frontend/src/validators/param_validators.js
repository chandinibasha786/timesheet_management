/**
 * Helper function to validate email string
 * checks if email is too short, else returns "email should be at least 6 characters long and max of 50"
 * checks if email is of the correct format, else returns "email should be of the correct format"
 * @param {} email
 * @returns
 */
const _email_validator = (email) => {
  if (email.length < 6) {
    return "Email length should be at least 6 characters long";
  }
  if (email.length > 50) {
    return "Email length should be a max of 50";
  }
  if (
    !email.match(
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
  ) {
    return "This is not a valid email.";
  }
  return "";
};

/**
 * Helper function to validate password string
 * checks if new_password is too short, else returns "new_password should be at least 10 characters long and max of 20"
 * checks if new_password is of the correct format, else returns "new_password should have at least 1 special character and at least 1 digit"
 * @param {} password
 * @returns
 */
 const _password_validator = (password) => {
  if (password.length < 10) {
    return "password length should be at least 10 characters long";
  }
  if (password.length > 15) {
    return "password length should be a max of 15";
  }
  if (!password.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{10,15}$/)) {
    return "Password should have at least 1 digit, one special character ";
  }

  return "";
};

/*
 * Helper function to validate security_answer string
 * checks if _security_answer_validator is string, else returns "security_answer should be of string"
 * @param {}security_answer
 * @returns
 */
const _security_answer_validator = (security_answer) => {
  if (typeof security_answer != "string") {
    return "security_answer should be of string type";
  }
  return "";
};

/**
 * Helper function to validate new_password string
 * checks if new_password is too short, else returns "new_password should be at least 10 characters long and max of 20"
 * checks if new_password is of the correct format, else returns "new_password should have at least 1 special character and at least 1 digit"
 * @param {} new_password
 * @returns
 */
 const _new_password_validator = (new_password) => {

  if (new_password.length < 10) {
    return "new_password length should be at least 10 characters long";
  }
  if (new_password.length > 15) {
    return "new_password length should be a max of 15";
  }
  if (
    !new_password.match(
      /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{10,15}$/
    )
  ) {
    return "new_password should be of the correct format";
  }

  return "";
};



//exporting all modules
export { _email_validator, _password_validator, _security_answer_validator,_new_password_validator  };
