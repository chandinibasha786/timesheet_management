//importing internal modules
import { ActionTypes } from "../Constants/actionTypes.js";

//initializing the state
const initialState = {
  loginData: {},
  profileData: {},
  timesheetData: []
};

//employeereducer function will set the login data and manipulate the state
export const employeeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_LOGIN_DATA:
      return { ...state, loginData: payload };

    default:
      return state;
  }
};

//employeeProfileReducer function will set the profile data and manipulate the state
export const employeeProfileReducer = (state = initialState.profileData, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_PROFILE_DATA:
      return {...state, profileData: payload };

      default:
        return state;
  }
};

//employeeTimesheetReducer function will set the timesheet data and manipulate the state
export const employeeTimesheetReducer = (state = initialState.timesheetData, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_TIMESHEET_DATA:
      return {...state, timesheetData: payload };

      default:
        return state;
  }
};
