//importing external module
import { combineReducers } from "redux";

//importing internal modules
import { employeeReducer,employeeProfileReducer,employeeTimesheetReducer } from "./Reducers.js";

//writing reducers function to combine all the reducers 
const reducers = combineReducers({
  employeeLogin: employeeReducer,
  employeeProfile: employeeProfileReducer,
  employeeTimesheet: employeeTimesheetReducer
});

//exporting reducers module
export default reducers;
