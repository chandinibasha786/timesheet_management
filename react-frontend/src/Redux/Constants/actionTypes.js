//exporting actiontypes module
export const ActionTypes = {
  SET_LOGIN_DATA: "SET_LOGIN_DATA",
  SET_PROFILE_DATA: "SET_PROFILE_DATA",
  SET_TIMESHEET_DATA: "SET_TIMESHEET_DATA"
}
