//importing internal module
import { ActionTypes } from "../Constants/actionTypes.js";

//writing a function to set the login data using actiontypes and exporting the module
export const setLoginData = (loginData) => {
  return {
    type: ActionTypes.SET_LOGIN_DATA,
    payload: loginData,
  };
};

//writing a function to set the profile data using actiontypes and exporting the module
export const setProfileData = (profile_data)=>{
  return{
      type: actionTypes.SET_PROFILE_DATA,
      payload: profile_data
  }
};

//writing a function to set the timesheet data using actiontypes and exporting the module
export const setTimesheetData = (timesheet_data)=>{
  return{
      type: actionTypes.SET_TIMESHEET_DATA,
      payload: timesheet_data
  }
};
