//imported external dependencies
import mongoose from "mongoose";

/**
 * login_schema
 *
 */
const LOGIN_SCHEMA = new mongoose.Schema({
  email: { type: String, required: true },
  password: { type: String, required: true },
  jwt_token: { type: String, required: true },
});

/**
 * forgot schema
 *
 */
const FORGOT_PASSWORD_SCHEMA = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  security_answer: { type: String, required: true },
  new_password: { type: String, required: true },
});

/**
 * profile schema
 */
const USER_PROFILE_SCHEMA = new mongoose.Schema({
  name: { type: String, required: true },
  empid: { type: String, required: true, unique:true },
  dateofjoining: { type: String, required: true },
  email: { type: String, required: true, unique:true },
  phoneNo: { type: Number, required: true, unique:true},
  role: { type: String, required: true },
  address: { type: String, required: true },
  password: { type: String, required: true },
  security_question: { type: String, required: true },
  security_answer: { type: String, required: true },
  jwt_token: { type: String },
});

/**
 *Timesheet Schema
 */
const TIMESHEET_SCHEMA = new mongoose.Schema({
  data: { type: String, required: true },
  project_name: { type: String, required: true },
  task_name: { type: String, required: true },
  start_time: { type: String, required: true },
  end_time: { type: String, required: true },
  description: { type: String},
});

export {
  LOGIN_SCHEMA,
  FORGOT_PASSWORD_SCHEMA,
  USER_PROFILE_SCHEMA,
  TIMESHEET_SCHEMA,
};
