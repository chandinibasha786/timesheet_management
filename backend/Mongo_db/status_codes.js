// defining the conflict_message it contains the object with respected status_code(409), status_message
var conflict_message = {
  status_code: 409,
  msg: "username already exists",
};

/**
 * @param {1} username it caontains username that are trying to login
 * @returns a object that contains status_code and status_message(406)
 */

function user_alredy_login(username) {
  return {
    status_code: 406,
    msg: `Already logged in as ${username}`,
  };
}

// exporing the conflict_message, user_alredy_login
export { conflict_message, user_alredy_login };
