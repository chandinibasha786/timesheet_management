//imported external dependencies
import mongoose from "mongoose";

//connecting mongoose
mongoose.connect(
  "mongodb+srv://npurimetla:npurimetla@cluster0.k2n1x.mongodb.net/TimeSheet?retryWrites=true&w=majority "
);

/**
 * importing shemas from schema.js
 */
import {
  LOGIN_SCHEMA,
  FORGOT_PASSWORD_SCHEMA,
  USER_PROFILE_SCHEMA,
  TIMESHEET_SCHEMA,
} from "./schema.js";

//creating a model for login
const UserLogin = mongoose.model("LOGIN_DATA", LOGIN_SCHEMA);

//creating a model for employee prfile
const UserModel = mongoose.model("PROFILE_DATA", USER_PROFILE_SCHEMA);

//creatimg a model for forgot password
const ForgotPasswordModel = mongoose.model(
  "FORGOT_DATA",
  FORGOT_PASSWORD_SCHEMA
);

//creating a model for timesheet schema
const TimeSheetModel = mongoose.model(
  "TIMESHEET_SCHEMA",
  TIMESHEET_SCHEMA
);

function inserting_user_login_data_into_database(login_data) {
  const user_login_data = new UserLogin({
    username: login_data.username,
    password: login_data.password,
    jwt_token: login_data.jwt_token,
  });
  // saving the data
  user_login_data
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}

function inserting_user_register_data_into_database(register_data) {
  const user_data = new UserModel({
    name: register_data.name,
    empid: register_data.empid,
    password: register_data.password,
    dateofjoining: register_data.dateofjoining,
    phoneNo: register_data.phoneNo,
    role: register_data.role,
    email: register_data.email,
    hobbies: register_data.hobbies,
    address: register_data.address,
    security_question: register_data.security_question,
    security_answer: register_data.security_answer  });
  // saving the data
  user_data
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
  response.send("data added successfully");
}


function updating_timesheet_data_to_datbase(time_sheet_data){
  const user_time_sheet = new TimeSheetModel({
    data:time_sheet_data.data,
    project_name:time_sheet_data.project_name,
    task_name:time_sheet_data.task_name,
    start_time:time_sheet_data.start_time,
    end_time:time_sheet_data.end_time,
    description:time_sheet_data.description
  })
  user_time_sheet
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
  response.send("data added successfully");

}

//exporting all models
export {
  UserLogin,
  ForgotPasswordModel,
  UserModel,
  TimeSheetModel,
  inserting_user_login_data_into_database,
  inserting_user_register_data_into_database,
  updating_timesheet_data_to_datbase
};
