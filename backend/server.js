//importing express
import express from "express";
//importing body-parser
import bodyParser from "body-parser";

//port number for the server 
const PORT = 3001;
//creating an instance of express
const app = express();
//returns middleware that only parses json
app.use(bodyParser.json());
//returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

//CORS enables you to access a resource from a different origin
import cors from "cors";
app.use(cors({origin:"*"}))

//imported middleware for further purpose 
import {
  login_data_validator,
  register_data_validator,
  forgotpassword_data_validator,
  register_keys_validator,
  time_sheet_validator
} from "./utils/validation/validators.js";

//imported middleware for further purpose 
import { get_user_profile_details } from "./path_handlers/get_apis.js";
import { register_handler, login_handler, time_sheet_handler } from "./path_handlers/post_apis.js";
import { forgot_password_handler } from "./path_handlers/put_apis.js";
import {get_all_user_details, get_time_sheet_handler_by_Id} from "./path_handlers/get_apis.js"
import {delete_user_profile_details} from "./path_handlers/delete_apis.js"
import {register_db_validator}  from "./utils/db/db_validators.js";
import { jwt_validator_admin, jwt_validator_employee, jwt_validator_hr } from "./utils/jwt/jwt_token_generator.js" ;


/**
 * @path /login path is tpo login for employee 
 * @param login_data_validator is the middleware that checks the params.
 * if the param matches the data get updated.
 * @param login_handler is the path_handler that checks for the user profile in database
 * if profile not matches with email and password provided throws an error
 * if email and password matches returns the login is successfull
 */
app.post("/login", login_data_validator, login_handler);


/**
 * @path /post_user_profile path to post profiles of employee.
 * @param register_data_validator is the middleware that checks the params.
 * if the param matches the data get updated.
 * @param register_keys_validator is the middleware that checks the keys.
 * if the keys matches the data provided then validation gores to next step.
 * @param register_db_validator is the middleware that checks the params in the backend
 * if the param matches the data get updated.
 * @param register_handler is the path_handler that checks for the user existed in database.
 * if already presents throws an error .
 * else crates an user profile.
 */
app.post("/post_user_profile", jwt_validator_admin, register_keys_validator, register_data_validator, register_db_validator, register_handler);


/**
 * @path /forgot_password path to change the password of employee
 * @param forgotpassword_data_validator is the middleware that checks the params.
 * if the param matches the data get updated.
 * @param forgot_password_handler is the path_handler that checks for the user existed in database
 * if present then update password and will get update in profile_datas database.
 */
app.put("/forgot_password", forgotpassword_data_validator, forgot_password_handler);


/**
 * @path /user/:id path to get profiles of employee
 * @param jwt_validator is the middleware that verifies the jwt token
 * @param get_user_profile_details is the path_handler that checks for the user existed in database
 * if present gets data from the profile_datas database.
 */
app.get("/user/:id", jwt_validator_employee, get_user_profile_details);


/**
 * @path /time_sheet_update path to get profiles of employee
 * @param login_data_validator is the middleware to check the login details for updating timesheet
 * @param time_sheet_handler is the handler which checks the login details.
 * if it matches the user is accessed to update the time sheet for the task done.
 */
app.post("/time_sheet_update",jwt_validator_employee, time_sheet_validator,time_sheet_handler);


/**
 * @path /user/:id path to get profiles of employee
 * @param get_user_profile_details is the path_handler that checks for the user existed in database
 * if present gets all users data from the profile_datas database.
 */
 app.get("/users",  get_all_user_details);


/**
 * @path /delete_user_details path to get profiles of employee
 * @param jwt_validator is the middleware that verifies the jwt token
 * @param delete_user_profile_details is the path_handler that checks for the user existed in database
 * if present then it deletes the user data from the profile_datas database.
 */
 app.delete("/delete_user/:id", jwt_validator_admin, delete_user_profile_details);


 /**
 * @path /get_timesheet_of_employee/:id path to get timesheet of employee
 * @param get_time_sheet_handler_by_Id is the path_handler that checks for the user existed in database
 * if present then it get the timesheet of the user data from the profile_datas database.
 */
app.get("/get_timesheet_of_employee/:id", get_time_sheet_handler_by_Id);


//port number to check the server is running or not 
app.listen(PORT, () => {console.log(`The port is running @ ${PORT}`);});
