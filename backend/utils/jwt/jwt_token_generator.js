// importing jwt from the jsonwebtoken for creating the jwttoken
import jwt from "jsonwebtoken";

const MYSECRET = "123456789N";

/**
 * it generates the jwt_token based on the payload
 * @param email is the email that are passed in login path request body
 * @returns it returns a jwt_token for every function call
 */
function jwt_token_generator(email, role) {
  //initializing the payload for a set of fields that you want to include in the token being generated
  var payload = {
    email: email,
    role: role,
  };
  //initializing jwtToken variable for creating jwt Token
  const jwtToken = jwt.sign(payload, MYSECRET);
  return jwtToken;
}


/**
 * jwt_validator_employee() is used for validating whether the user is logged in or not as an employee role
 * @param {*} req authorization_token  from headers
 * @param {*} res
 * @param {*} next its an express midlleware
 */
 function jwt_validator_employee(req, res, next) {
   jwt_validator_helper(req, res, next, ["Employee", "HR", "Admin"])
  }

/**

 * jwt_validator_hr() is used for validating whether the user is logged in or not as an HR role
 * @param {*} req authorization_token  from headers
 * @param {*} res
 * @param {*} next its an express midlleware
 */
 function jwt_validator_hr(req, res, next) {
   jwt_validator_helper(req, res, next, ["HR", "Admin"])
}


/**

 * jwt_validator_admin() is used for validating whether the user is logged in or not as an Admin role
 * @param {*} req authorization_token  from headers
 * @param {*} res
 * @param {*} next its an express midlleware
 */
 function jwt_validator_admin(req, res, next) {
   jwt_validator_helper(req, res, next, ["Admin"])
}


function jwt_validator_helper(req, res, next, array_of_roles) {
  const authentication_token = req.headers.authorization;
  if (authentication_token !== undefined) {
    const jwt_token = req.headers.authorization.replace('Bearer ', '')
    jwt.verify(jwt_token, MYSECRET, (error, payload) => {
      if (error) {
        res.status(401).send("Invalid Jwt Token");
      }else {
        let count = 0;
        for (let role of array_of_roles) {
          count += 1
        }
        if (count !== 0) {
          req.email = payload.email;
          req.role = payload.role;
          next();
        }else {
          res.status(401).send("Unauthorized role, your role is " + payload.role + " and not in " + array_of_roles)
      }}})}}



// exporting the jwt_token_generator module
export { jwt_token_generator, jwt_validator_admin, jwt_validator_employee, jwt_validator_hr };
