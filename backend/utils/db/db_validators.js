// importing UserModel model
import { UserModel } from "../../Mongo_db/model.js";

/**
 * register_validator() is used for unique email, empid, phoneNO
 * @param {*} req email, empid, phoneNO as body params and checks for uniqueness
 * @param {*} res if the params already exists in database sends error status_code and status_message
 * @param {*} next it is middleware which works when all the conditions gets satisfied
 */
function register_db_validator(req, res, next) {
  const { email, empid, phoneNo } = req.body;

  
  // checking whether the employee id is unique or not
  UserModel.findOne(
    {
      $or: [
        { email: `${email}` },
        { empid: `${empid}` },
        { phoneNo: `${phoneNo}` },
      ],
    },
    (error, data) => {
      if (error) {
        res
          .status(403)
          .send({
            msg: "Error in finding user by email, empid, phoneNo",
          });
      } else {
        if (data == null) {
          next();
        } else {
          // we have at least 1 match
          res
            .status(403)
            .send({
              msg:
                "email, phoneNo and empid should be unique " +
                JSON.stringify({ email, empid, phoneNo }),
            });
        }
      }
    }
  );
}

//exporting register-db_validator module
export { register_db_validator };
