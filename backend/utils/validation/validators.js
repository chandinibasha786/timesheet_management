/**
 * This is a helper middleware function to help with validation of login
 * parameters.
 * @param {}  req   HTTP req object as recieved by Express backend
 * @param {*} res   HTTP response object to be populated by Express backend
 * @param {*} next  next() is a function to be called if this middlware validation is success
 * @returns
 */
const login_data_validator = (req, res, next) => {
  const { email, password } = req.body;
  const error_object = {};

  error_object.email = _email_helper(email).email;
  error_object.password = _password_helper(password).password;

  // if (Object.getOwnPropertyNames(error_object).length > 0) {
  //   res.status(200).send(error_object)
  // }

  const arr = [];
  for (const key in error_object) {
    if (typeof error_object[key] !== "undefined") {
      arr.push(error_object[key]);
    }
  }

  if (arr.length !== 0) {
    res.status(406).send(error_object);
  }else{
    next()
  }
  // else {
  //   res.send("validation Succesfull")
  // }
};

/**
 * This is a helper middleware function to help with validation of register
 * parameters.
 * @param {}  req   HTTP req object as recieved by Express backend
 * @param {*} res   HTTP response object to be populated by Express backend
 * @param {*} next  next() is a function to be called if this middlware validation is success
 * @returns
 */
const register_data_validator = (req, res, next) => {
  const {
    name,
    empid,
    dateofjoining,
    email,
    phoneNo,
    role,
    address,
    security_question,
    password,
    security_answer  } = req.body;

  const error_object = {};

  error_object.name = _name_helper(name).name;
  error_object.email = _email_helper(email).email;
  error_object.empid = _empid_helper(empid).empid;
  error_object.dateofjoining =
    _date_of_joining_helper(dateofjoining).dateofjoining;
  error_object.phoneNo = _phoneNo_helper(phoneNo).phoneNo;
  error_object.role = _role_helper(role).role;
  error_object.address = _address_helper(address).address;
  error_object.password = _password_helper(password).password;
  error_object.security_question =
    _security_question_helper(security_question).security_question;
  //console.log(`error obj ${error_object}`);
  error_object.security_answer =
    _security_answer_helper(security_answer).security_answer;

  // if (Object.getOwnPropertyNames(error_object).length > 0) {
  //   res.status(200).send(error_object)
  // }

  const arr = [];
  for (const key in error_object) {
    if (typeof error_object[key] !== "undefined") {
      arr.push(error_object[key]);
    }
  }

  if (arr.length !== 0) {
    res.status(406).send(error_object);
  } else {
    next();
  }

};

/**
 * This is a helper middleware function to help with validation of forgot password
 * parameters.
 * @param {}  req   HTTP req object as recieved by Express backend
 * @param {*} res   HTTP response object to be populated by Express backend
 * @param {*} next  next() is a function to be called if this middlware validation is success
 * @returns
 */
const forgotpassword_data_validator = (req, res, next) => {
  const { email, security_answer, new_password } = req.body;

  const error_object = {};

  error_object.email = _empid_helper(email).email;
  error_object.security_answer =
    _security_answer_helper(security_answer).security_answer;
  error_object.new_password = _new_password_helper(new_password).new_password;

  // if (Object.getOwnPropertyNames(error_object).length > 0) {
  //   res.status(200).send(error_object);
  // }

  const arr = [];
  for (const key in error_object) {
    if (typeof error_object[key] !== "undefined") {
      arr.push(error_object[key]);
    }
  }

  if (arr.length !== 0) {
    res.status(406).send(error_object);
  }

  next();
};

/**
 * this is a helper middleware function to help validation for keys in register
 * @param {*} request is used to check the keys all are in correct or not 
 * @param {*} response send an error or success message based on the req done 
 * @param {*} next next() is a function to be called if this middlware validation is success
 * @returns 
 */
const register_keys_validator = (request, response, next) => {
  // checking every param if anyone of them is not provided-'406' :not acceptable
  if (Object.keys(request.body).length !== 10) {
    //if required number of params are not there it will send 421 status code
    return response.status(406).send(
      JSON.stringify({  msg: "Parameters count is not matches with database fields" })
    );
  }

  let body_keys_list = [];

  for (let item in request.body) {
    body_keys_list.push(item);
  }

  //delaring a list of keys to validate the keys

  let key_list = [
    "role",
    "empid",
    "password",
    "dateofjoining",
    "email",
    "phoneNo",
    "security_question",
    "security_answer",
    "name",
    "address",
  ];

  let msg = "";
  for (let key of key_list) {
    if (body_keys_list.includes(key)) {
      continue;
    } else {
      msg += key + " is Missing!\n";
    }
  }

  if (msg.length != 0) {
    return response.status(406).send(JSON.stringify({ msg:msg }));
  } else {
    next();
  }
};

/**
 * This is a helper middleware function to help with validation of register
 * parameters.
 * @param {}  req   HTTP req object as recieved by Express backend
 * @param {*} res   HTTP response object to be populated by Express backend
 * @param {*} next  next() is a function to be called if this middlware validation is success
 * @returns
 */
 const time_sheet_validator = (req, res, next) => {
  const {
    data,
    project_name,
    task_name,
    start_time,
    end_time,
    description
  } = req.body;
  const error_object = {};

  error_object.data = _data_helper(data).data;
  error_object.project_name = _project_name_helper(project_name).project_name;
  error_object.task_name = _task_name_helper(task_name).task_name;
  error_object.start_time =
  _start_time_helper(start_time).start_time;
  error_object.end_time = _end_time_helper(end_time).end_time;
  error_object.description = _description_helper(description).description;


  // if (Object.getOwnPropertyNames(error_object).length > 0) {
  //   res.status(200).send(error_object)
  // }

  const arr = [];
  for (const key in error_object) {
    if (typeof error_object[key] !== "undefined") {
      arr.push(error_object[key]);
    }
  }

  if (arr.length !== 0) {
    res.status(406).send(error_object);
  }

  next();
};

/**************************************** register ***************************************/

/**
 * Helper function to validate password string
 * checks if password is string, else returns "password should be of string type"
 * checks if password is too short, else returns "password should be at least 10 characters long and max of 20"
 * checks if password is of the correct format, else returns "password should have at least 1 special character and at least 1 digit"
 * @param {} password
 * @returns
 */
const _password_helper = (password) => {
  const returnObject = {};
  // is it a string
  if (typeof password != "string") {
    returnObject.password = "password should be of string type";
    return returnObject;
  }
  if (password.length < 10) {
    returnObject.password =
      "password length should be at least 10 characters long";
    return returnObject;
  }
  if (password.length > 20) {
    returnObject.password = "password length should be a max of 20";
    return returnObject;
  }
  if (
    !password.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{10,20}$/)
  ) {
    returnObject.password = "password should be of the correct format";
    return returnObject;
  }

  return returnObject;
};

/**
 * Helper function to validate name string
 * checks if name is string, else returns "name should be of string type"
 * checks if name is too short, else returns "name should be at least 4 characters long and max of 25"
 * checks if name is of the correct format, else returns "name should not have at least 1 special character and at least 1 digit"
 * @param {} name
 * @returns
 */
const _name_helper = (name) => {
  const returnObject = {};

  if (typeof name != "string") {
    returnObject.password = "name should be of string type";
    return returnObject;
  }

  if (name.length < 3) {
    returnObject.name = "name length should be at least 4 characters long";
    return returnObject;
  }

  if (name.length > 25) {
    returnObject.name = "name length shouldn't be 25 characters long";
    return returnObject;
  }

  if (!name.match(/^[a-zA-Z ]+$/)) {
    returnObject.name = "name should be of the correct format";
    return returnObject;
  }

  return returnObject;
};

/**
 * Helper function to validate email string
 * checks if email is string, else returns "email should be of string type"
 * checks if email is too short, else returns "email should be at least 6 characters long and max of 50"
 * checks if email is of the correct format, else returns "email should be of the correct format" 406
 * @param {} email
 * @returns
 */
const _email_helper = (email) => {
  const returnObject = {};
  // is it a string
  if (typeof email != "string") {
    returnObject.email = "email should be of string type";
    return returnObject;
  }
  if (email.length < 6) {
    returnObject.email = "email length should be at least 6 characters long";
    return returnObject;
  }
  if (email.length > 50) {
    returnObject.email = "email length should be a max of 50";
    return returnObject;
  }
  if (
    !email.match(
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )
  ) {
    returnObject.email = "email should be of the correct format";
    return returnObject;
  }

  return returnObject;
};

/**
 * Helper function to validate empid string
 * checks if empid is string, else returns "empid should be of string type"
 * checks if empid is too short, else returns "empid should be at least 6 characters long and max of 15"
 * checks if empid is of the correct format, else returns "empid should not have at least 1 special character"
 * @param {} empid
 * @returns
 */
const _empid_helper = (empid) => {
  const returnObject = {};

  if (typeof empid != "string") {
    returnObject.empid = "empid should be of string type";
    return returnObject;
  }

  if (empid.length < 3) {
    returnObject.empid = "empid length should be at least 6 characters long";
    return returnObject;
  }

  if (empid.length > 15) {
    returnObject.empid =
      "empid length should not be greater than 15 characters long";
    return returnObject;
  }

  // if (!(empid.match(/^([A-Za-z]{3,}-\d+;)+$/))) {
  //   returnObject.empid = "empid should be of the correct format"
  //   return returnObject;
  // }

  return returnObject;
};

/**
 * Helper function to validate dateofjoining string
 * checks if dateofjoining is string, else returns "dateofjoining should be of string type"
 * checks if dateofjoining is too short, else returns "dateofjoining should be at least 6 characters long and max of 15"
 * checks if dateofjoining is of the correct format, else returns "dateofjoining should not have at least 1 special character"
 * @param {} dateofjoining
 * @returns
 */
const _date_of_joining_helper = (dateofjoining) => {
  const returnObject = {};

  if (typeof dateofjoining != "string") {
    returnObject.dateofjoining = "dateofjoining should be of string type";
    return returnObject;
  }

  if (dateofjoining.length < 4) {
    returnObject.dateofjoining =
      "dateofjoining length should be at least 6 characters long";
    return returnObject;
  }

  if (dateofjoining.length > 21) {
    returnObject.dateofjoining =
      "dateofjoining length should have 8 characters long";
    return returnObject;
  }

  // if (!(dateofjoining.match(/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/))) {
  //   returnObject.dateofjoining = "dateofjoining should be of the correct format"
  //   return returnObject;
  // }

  return returnObject;
};

/**
 * Helper function to validate phone number
 * checks if phone no is number, else returns "phone number should be of number type"
 * checks if phone no is too short, else returns "phone number should be at least 1 characters long and max of 10"
 * checks if phone no is of the correct format, else returns "phone number should be of the correct format" 406
 * @param {} phoneNo
 * @returns
 */
const _phoneNo_helper = (phoneNo) => {
  const returnObject = {};

  if (typeof phoneNo != "number") {
    returnObject.phoneNo = "phone number should be of number type";
    return returnObject;
  }

  if (phoneNo.toString().length < 10) {
    returnObject.phoneNo =
      "phone number length should be at least 10 characters long";
    return returnObject;
  }

  if (phoneNo.length > 10) {
    returnObject.phoneNo = "phone number length should be a max of 10";
    return returnObject;
  }

  // if (!(phoneNo.match('/\b([0-9]|10)\b /'))) {
  //   returnObject.phoneNo = "phone number should be of the correct format"
  //   return returnObject;
  // }
  return returnObject;
};

/**
 * Helper function to validate role
 * checks if role is string, else returns "role should be of string type"
 * checks if role is too short, else returns "role should be at least 1 characters long and max of 50"
 * checks if role is of the correct format, else returns "role should be of the correct format" 406
 * @param {} role
 * @returns
 */
const _role_helper = (role) => {
  const returnObject = {};
  if (typeof role != "string") {
    returnObject.role = "role should be of string type";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate address string
 * checks if address is string, else returns "address should be of string type"
 * @param {} address
 * @returns
 */
const _address_helper = (address) => {
  const returnObject = {};
  if (typeof address != "string") {
    returnObject.address = "address should be of string type";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate security_question string
 * checks if security_question is string, else returns "security_question should be of string"
 * @param {} security_question * @returns
 */
const _security_question_helper = (security_question) => {
  const returnObject = {};

  if (typeof security_question != "string") {
    returnObject.security_question =
      "security_question should be of string type";
    return returnObject;
  }

  return returnObject;
};

/*
 * Helper function to validate security_answer string
 * checks if security_question is string, else returns "security_answer should be of string"
 * @param {}security_answer
 * @returns
 */
const _security_answer_helper = (security_answer) => {
  const returnObject = {};
  if (typeof security_answer != "string") {
    returnObject.security_answer = "security_answer should be of string type";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate new_password string
 * checks if new_password is string, else returns "new_password should be of string type"
 * checks if new_password is too short, else returns "new_password should be at least 10 characters long and max of 20"
 * checks if new_password is of the correct format, else returns "new_password should have at least 1 special character and at least 1 digit"
 * @param {} new_password
 * @returns
 */
const _new_password_helper = (new_password) => {
  const returnObject = {};
  // is it a string
  if (typeof new_password != "string") {
    returnObject.new_password = "new_password should be of string type";
    return returnObject;
  }
  if (new_password.length < 10) {
    returnObject.new_password =
      "new_password length should be at least 10 characters long";
    return returnObject;
  }
  if (new_password.length > 15) {
    returnObject.new_password = "new_password length should be a max of 15";
    return returnObject;
  }
  if (
    !new_password.match(
      /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{10,15}$/
    )
  ) {
    returnObject.new_password = "new_password should be of the correct format";
    return returnObject;
  }

  return returnObject;
};

/**
 * Helper function to validate data string in timesheet
 * checks if data is string, else returns "data should be of string type"
 * @param {} data
 * @returns
 */
 const _data_helper = (data) => {
  const returnObject = {};
  if (typeof data != "string") {
    returnObject.data = "data should be of string type";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate project_name string in timesheet
 * checks if project_name is string, else returns "project_name should be of string type"
 * @param {} project_name
 * @returns
 */
 const _project_name_helper = (project_name) => {
  const returnObject = {};
  if (typeof project_name != "string") {
    returnObject.project_name = "project_name should be of string type";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate task_name string in timesheet
 * checks if task_name is string, else returns "task_name should be of string type"
 * @param {} task_name
 * @returns
 */
 const _task_name_helper = (task_name) => {
  const returnObject = {};
  if (typeof task_name != "string") {
    returnObject.task_name = "task_name should be of string type";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate start_time string in timesheet
 * checks if start_time is string, else returns "start_time should be of string type"
 * @param {} start_time
 * @returns
 */
 const _start_time_helper = (start_time) => {
  const returnObject = {};
  if (typeof start_time != "string") {
    returnObject.start_time = "start_time should be of string type";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate end_time string in timesheet
 * checks if end_time is string, else returns "end_time should be of string type"
 * @param {} end_time
 * @returns
 */
 const _end_time_helper = (end_time) => {
  const returnObject = {};
  if (typeof end_time != "string") {
    returnObject.end_time = "end_time should be of string type";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate description string in timesheet
 * checks if description is string, else returns "description should be of string type"
 * @param {} description
 * @returns
 */
 const _description_helper = (description) => {
  const returnObject = {};
  if (typeof description != "string") {
    returnObject.description = "description should be of string type";
    return returnObject;
  }
  return returnObject;
};

export {
  login_data_validator,
  register_data_validator,
  forgotpassword_data_validator,
  register_keys_validator,
  time_sheet_validator
};
