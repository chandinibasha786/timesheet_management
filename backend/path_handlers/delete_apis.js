//importing module 
import { UserModel } from "../Mongo_db/model.js";

/**
 * @param delete_user_profile_details gets the details of an user using JWT token
 * @param {*} request HTTP req object as recieved by Express backend
 * @param {*} response HTTP response object to be populated by Express backend
 */

const delete_user_profile_details = (request, response) => {
  const jwt_token = request.headers.authorization.split(" ")[1];

  UserModel.deleteOne({ jwt_token: jwt_token }, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      response.send("user deleted successfully");
    }
  });
};

export {delete_user_profile_details}