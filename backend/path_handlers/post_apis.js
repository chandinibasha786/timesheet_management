// importing model
import { UserModel, UserLogin, TimeSheetModel} from "../Mongo_db/model.js";
//importing jwt token generator
import { jwt_token_generator } from "../utils/jwt/jwt_token_generator.js";

/**
 * 
 * @param {*} req HTTP req object as recieved by Express backend
 * @param {*} res HTTP response object to be populated by Express backend
 * @param {*} next next() is a function to be called if this middlware validation is success
 */
function login_handler(req, res, next) {
  const { email, password } = req.body;

  // Checking whether the employee email exists or not
  UserModel.findOne({ email: email }, (err, data) => {
    if (err) {
      res
        .status(406)
        .send({ msg: "Error in finding user by email" });
    } else {
      if (data) {

        if (data.password === password) {
          const email = data.email;
          const role = data.role;
          const jwt_token = jwt_token_generator(email, role);

          if (jwt_token !== "undefined") {
            res.send({ email, role, jwt_token })
          }
          // if jwt_token is not generated then sends response as login unsuccessful
          else {
            res.status(400).send({ msg: "log in unsuccessfull" });
          }
        }
        // if the password doesn't matches sends 404 as response
        else {
          res.status(404).send({ msg: "invalid password" });
        }
      }
      else {// if user doesn't exists sends 404 as response
        res.status(404).send({ msg: "email doesn't exists" });
      }
    }
  }).select('email password role');
}


/**
 * 
 * @param {*} req HTTP req object as recieved by Express backend
 * @param {*} res HTTP response object to be populated by Express backend
 * @param {*} next next() is a function to be called  when all the keys are validated.
 */
function register_handler(req, res, next) {
  const {
    name,
    empid,
    dateofjoining,
    email,
    phoneNo,
    role,
    address,
    password,
    security_question,
    security_answer  } = req.body;

  const data = {
    name,
    empid,
    dateofjoining,
    email,
    phoneNo,
    role,
    address,
    password,
    security_question,
    security_answer  };

  const user_data = new UserModel(data);

  UserModel.find(
    {
      $or: [{ email: email }, { empid: empid }, {phoneNo: phoneNo}],
    },
    (err, data) => {
      if (err) {
        return res
          .status(406)
          .send({ message: "Error in finding user by employee id" });
      }
      if (data.length !== 0) {
        if (data[0].email === email) {
          return res
            .status(402)
            .send(JSON.stringify({ message: "Email ID already exists" }));
        }
      }
      if (data.length == 0) {
        user_data.save().then(() => {
          return res
            .status(201)
            .send({ message: "User Registration Successfull" });
        });
      } else {
        return res.status(409).send({ message: "employee ID already exists" });
      } 
    }
  );
}


/**
 * 
 * @param {*} req HTTP req object as recieved by Express backend
 * @param {*} res HTTP response object to be populated by Express backend
 * @param {*} next next() is a function to be called when the time sheet gets updated.
 */
function time_sheet_handler(req, res ,next) {
  const {
    data,
    project_name,
    task_name,
    start_time,
    end_time,
    description
  } = req.body;

  const time_sheet_data = {data,
    project_name,
    task_name,
    start_time,
    end_time,
    description};

  const user_time_sheet_data = new TimeSheetModel(time_sheet_data);
  user_time_sheet_data.save().then(() => {
    res.status(201).send({ message: "Timesheet Updated successfully" });
  });
}


//exporting handler modules
export { register_handler, login_handler, time_sheet_handler };
