
//importing module
import { UserModel } from "../Mongo_db/model.js";

/**
 * @param get_user_profile_details gets the details of only specified user using JWT token
 * @param {*} request HTTP req object as recieved by Express backend
 * @param {*} response HTTP response object to be populated by Express backend
 */

const get_user_profile_details = (request, response) => {
  const jwt_token = request.headers.authorization.split(" ")[1];

  UserModel.find({ jwt_token: jwt_token }, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      response.send(data);
    }
  });
};

/**
 * @param get_all_user_details gets the details of all users.
 * @param {*} request HTTP req object as recieved by Express backend
 * @param {*} response HTTP response object to be populated by Express backend
 */
const get_all_user_details = (request,response) => {
  //const data = new UserModel(register_handler);

  UserModel.find({}, (error,data) => {
      if (error) {
        console.log(error)
      } else {
        // send the data to the response
        response.send(data)
      }
    })
}

/**
 * fetch timesheet details with respect to id
 * @param {*} request is the request by the user
 * @param {*} response is the response with respect to the user
 */
 const get_time_sheet_handler_by_Id = (request, response) => {
  let id = request.params.id;
  TimeSheetData.find({ employeeId: id })
  .then((data) => {
    response.send(data);
  })
  .catch((err) => {
    console.log(err);
  });
}

//exporting modules for importing
export { get_user_profile_details, get_all_user_details, get_time_sheet_handler_by_Id};
