//importing model
import { UserModel } from "../Mongo_db/model.js";

/**
 * 
 * @param {*} req HTTP req object as recieved by Express backend
 * @param {*} res HTTP response object to be populated by Express backend
 * @param {*} next next() is a function to be called if this middlware validation is success
 */

function forgot_password_handler(req, res, next) {
  const { email, security_answer, new_password } = req.body;

  UserModel.findOne({ email: email }, (err, data) => {
    if (err) {
      res.status(404).send({ message: "Error in finding user by email" });
    } else {
      if (data) {
        if (data.security_answer === security_answer) {
          UserModel.updateOne(
            { email: email },
            { $set: { password: new_password } }
          )
            .then(() => {
              res
                .status(200)
                .send({ message: " Password updated successfully" });
            })
            .catch(() => {
              res.status(304).send({ message: "Unable to update database" });
            });
        } else {
          res.status(401).send({ message: "security answer is not valid" });
        }
      } else {
        res.status(404).send({ message: "User doesn't exist" });
      }
    }
  });
}

export { forgot_password_handler };
